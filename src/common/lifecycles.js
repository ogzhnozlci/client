import { useEffect, useRef } from "react";

export const componentDidMount = handler => {
    return useEffect(() => {
        return handler();
    }, []);
};

export const componentDidUpdate = (handler, deps) => {
    const isInitialMount = useRef(true);

    return useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false;
            return;
        }

        return handler();
    }, deps);
};


export const componentWillUnmount = handler => {
    return useEffect(() => {
        return () => {
            return handler();
        };
    }, []);
};
