import React, { useState } from 'react';
import {useDispatch} from 'react-redux'
import { GetData, Nodes } from "Store";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import TaskList from "../../components/TaskList/TaskList";

import {componentDidMount, componentDidUpdate} from "../../common/lifecycles";
import Actions from "../../redux/actions";

const Task = (props) => {
    const dispatch = useDispatch()
    const taskData = GetData(Nodes.task)
    const [tasks, setTasks] = useState([])

    componentDidMount(()=>{
        dispatch(Actions.task.getAllTasks());
    })

    componentDidUpdate(()=> {
        setTasks(taskData.all_task);
    }, [taskData.all_task])

    return <div className="Task">
                <Container>
                    <Row>
                        <Col>
                            <h1 className={"text-center"}>
                                TASKS
                            </h1>
                        </Col>
                    </Row>
                    <TaskList data={tasks}/>
                </Container>

            </div>
}

export default Task;
