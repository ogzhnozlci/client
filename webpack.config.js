const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require("autoprefixer");

module.exports = (env, argv ) => {

    let _ENV = "PRODUCTION";

    if (env.test) {
        _ENV = "TEST"
    }

    let _IS_PRODUCTION = true;

    if (argv.mode === 'development') {
        _IS_PRODUCTION = false;
    }

    const config = {
        entry: "./src/index.js",
        output: {
            publicPath: "/",
            path: path.resolve(__dirname, "dist"),
            filename: '[name]_[contenthash].js',
            assetModuleFilename: 'assets/[name][ext][query]'
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.scss$/,
                    use: [
                        _IS_PRODUCTION ? MiniCssExtractPlugin.loader : "style-loader",
                        "css-loader",
                        "postcss-loader",
                        "sass-loader"
                    ]
                },
                {
                    test: /\.json$/,
                    type: 'javascript/auto',
                    use: {
                        loader: 'json-loader'
                    }
                },
                {
                    test: /\.(png|svg|jpg|jpeg|gif)$/i,
                    type: 'asset/resource',
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/i,
                    type: 'asset/resource',
                    generator: {
                        filename: 'assets/fonts/[name]__[hash][ext]'
                    }
                },
            ]
        },
        plugins: [
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                template: "./src/index.html"
            }),
            new webpack.LoaderOptionsPlugin({
                options: {
                    postcss: [
                        autoprefixer()
                    ]
                }
            }),
            new MiniCssExtractPlugin({
                filename: "[name]_[contenthash].css",
            }),
            new webpack.DefinePlugin({
                'process.env.DOMAIN': JSON.stringify(_ENV)
            })
        ],
        resolve: {
            alias: {
                Lifecycles: path.resolve(__dirname, 'src/common/lifecycles.js'),
                Actions: path.resolve(__dirname, 'src/redux/actions/'),
                Reducers: path.resolve(__dirname, 'src/redux/reducers/'),
                Store: path.resolve(__dirname, 'src/redux/Store.js'),
                Storager: path.resolve(__dirname, 'src/common/storager.js'),
                Tools: path.resolve(__dirname, 'src/common/tools.js'),
                Images: path.resolve(__dirname, 'src/asset/images/'),
                Components: path.resolve(__dirname, 'src/components/'),
                Modals: path.resolve(__dirname, 'src/modals/'),
                Constants: path.resolve(__dirname, 'src/common/constants.js'),
                XHR: path.resolve(__dirname, 'src/common/xhr.js'),
                Notifications: path.resolve(__dirname, 'src/common/notifications.js'),
            }
        },
        devServer: {
            hot: true,
            open: true,
            port: 4000,
            historyApiFallback: true,
            allowedHosts: [
                'player-fizy.fizybusiness.com',
            ]
        },
        devtool: _IS_PRODUCTION ? 'source-map' : 'eval-source-map'
    }

    return config
}
