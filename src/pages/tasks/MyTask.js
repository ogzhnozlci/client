import React, { useState } from 'react';
import {useDispatch} from 'react-redux'
import { GetData, Nodes } from "Store";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import ReqestedTaskList from "../../components/TaskList/ReqestedTaskList";

import {componentDidMount, componentDidUpdate} from "../../common/lifecycles";
import Actions from "../../redux/actions";

const MyTask = (props) => {
    const dispatch = useDispatch()
    const taskData = GetData(Nodes.task)
    const [tasks, setTasks] = useState([])

    componentDidMount(()=>{
        dispatch(Actions.task.getMyTasks());
    })

    componentDidUpdate(()=> {
        setTasks(taskData.my_tasks);
    }, [taskData.my_tasks])

    return <div className="TaskRequested">
                <Container>
                    <Row>
                        <Col>
                            <h1 className={"text-center"}>
                                MY TASKS
                            </h1>
                        </Col>
                    </Row>
                    {
                        tasks.length !== 0 ?
                            <ReqestedTaskList data={tasks} isOwn={true}/>
                            :
                            <Row>
                                <Col>
                                    <h2 className={"text-center"}>There is no task</h2>
                                </Col>
                            </Row>
                    }

                </Container>

            </div>
}

export default MyTask;
