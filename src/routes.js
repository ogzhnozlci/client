import { USER_TYPE } from "./common/constants";

import Login from './pages/login/Login'
import Register from './pages/register/Register'

import Task from "./pages/tasks/Task";
import MyTask from "./pages/tasks/MyTask";
import CreateTask from "./pages/tasks/CreateTask";

export const Routes = (userType) => {
    let routes;
    switch(userType) {
        case USER_TYPE.TASKER:
            routes = [
                {
                    path: "/",
                    component: Task
                },
                {
                    component: Task
                },
            ]
            break;
        case USER_TYPE.CUSTOMER:
            routes = [
                {
                    path: "/",
                    component: Task
                },
                {
                    path: "/mytasks",
                    component: MyTask
                },,
                {
                    path: "/createtask",
                    component: CreateTask
                },
                {
                    component: Task
                },
            ]
            break;
        default:
         routes = [
             {
                 path: "/",
                 component: Login
             },
             {
                 path: "/register",
                 component: Register
             },
             {
                 component: Login
             },
         ]
    }



    return routes
}
