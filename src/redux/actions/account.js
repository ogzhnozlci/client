import { XHR } from 'XHR';
import axios from "axios";
import { ACCOUNT } from "../action_types";
import { isReqSuccess } from "Tools";

const registerAccount = (data) => {
    return async function(dispatch, getState) {

        let resp = await XHR.post("/user/register", data);

        dispatch({
            type: ACCOUNT.REGISTER_LOGIN,
            payload: resp.data.data
        })
    };
}
const loginAccount = (data) => {
    return async function(dispatch, getState) {

        let resp = await XHR.post("/user/login", data);

        dispatch({
            type: ACCOUNT.REGISTER_LOGIN,
            payload: resp.data.data
        })
    };
}

export default {
    registerAccount,
    loginAccount,
}
