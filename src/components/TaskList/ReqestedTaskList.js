import React, { useState, Fragment } from 'react';
import {useDispatch} from 'react-redux'
import { GetData, Nodes } from "Store";


import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

import TaskListItem from "./TaskListItem";
import {componentDidUpdate} from "../../common/lifecycles";
import Actions from "../../redux/actions";


const ReqestedTaskList = (props) => {
    const { data, isOwn, } = props;
    const dispatch = useDispatch();
    const taskData = GetData(Nodes.task)

    const [showRequetsModal, setShowRequetsModal] = useState(false);
    const [modalContent, setModalContent] = useState(false);
    const [modalContentRequests, setModalContentRequests] = useState(false);

    const handleClose = () => setShowRequetsModal(false);
    const handleShow = data => {
        setModalContent(data)
        dispatch(Actions.task.getRequestOfTask(data._id));
        setShowRequetsModal(true);
    }

    componentDidUpdate(()=>{
        setModalContentRequests(taskData.requests_of_task)
    },[taskData.requests_of_task])

    return <div className="TaskList">
                {
                    data.map((task,i)=>{
                        return <TaskListItem onBtnClick={handleShow} data={task} showRequest={true} isOwn={isOwn} key={i}/>
                    })
                }

                <Modal show={showRequetsModal} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{modalContent.title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {
                            modalContentRequests && modalContentRequests.length !== 0 ?
                                <Fragment>
                                    {
                                        modalContentRequests.map((req,i)=> {
                                            return (
                                                <div>
                                                    {req.request_creator.username + " requested: " + req.value + " " + req.currency}
                                                </div>
                                            )
                                        })
                                    }
                                </Fragment>
                                :
                                <h4>
                                    There is no Request for this Task
                                </h4>
                        }
                    </Modal.Body>
                </Modal>
            </div>
}

export default ReqestedTaskList;
