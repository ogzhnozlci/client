/**
 * @module common/tools
 * @desc Contains utility functions for common usage.
 */



const successStatusCodes = [200,201,204]
/**
 * Check a XHR Success
 * @method
 * @param {Number} responseXHR - response of XHR
 * @return {Boolean}
 */
export const isReqSuccess = (responseXHR) => {
    return successStatusCodes.indexOf(responseXHR.status) !== -1
}

/**
 * Check variable is an Object
 * @method
 * @param {Object} variable - Variable
 * @return {Boolean}
 */
export const isObject = (variable) => {
    return variable != null && typeof variable === 'object';
}

/**
 * Check two object is same (deep)
 * @method
 * @param {Object} object1
 * @param {Object} object2
 * @return {Boolean}
 */
export const deepEqual = (object1, object2) => {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);

    if (keys1.length !== keys2.length) {
        return false;
    }

    for (const key of keys1) {
        const val1 = object1[key];
        const val2 = object2[key];
        const areObjects = isObject(val1) && isObject(val2);
        if (
            areObjects && !deepEqual(val1, val2) ||
            !areObjects && val1 !== val2
        ) {
            return false;
        }
    }

    return true;
}

/**
 * get current route parameters from props in a react component
 * @method
 * @param {Object} props - props of component
 * @return {Object} current route paramters
 */
export const getParamsFromProps = (props) => {
    return props.match.params;
}
