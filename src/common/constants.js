export const BASE_URL = "http://localhost:3000"

export const USER_TYPE = {
    TASKER: "tasker",
    CUSTOMER: "customer"
}
