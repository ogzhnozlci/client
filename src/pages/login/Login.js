import React, { useState } from 'react';
import {useDispatch} from 'react-redux'
import { GetData, Nodes } from "Store";
import Actions from "../../redux/actions";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

import { componentDidMount } from "../../common/lifecycles";

const Login = (props) => {
    const { t } = props;
    const dispatch = useDispatch()
    const loginData = GetData(Nodes.account)

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    componentDidMount(()=>{

    })

    const validateForm = () => {
        return email.length > 0 && password.length > 5;
    }

    const handleSubmit = (event) => {
        let data = {
            email,
            password,
        }
        dispatch(Actions.account.loginAccount(data))
        event.preventDefault();
    }

    return <div className="Login">
                <h1>
                    LOGIN
                </h1>
                <Form onSubmit={handleSubmit}>
                    <Form.Group size="lg" controlId="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            type="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group size="lg" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </Form.Group>
                    <Button block size="lg" type="submit" disabled={!validateForm()}>
                        Login
                    </Button>
                </Form>
            </div>
}

export default Login;
