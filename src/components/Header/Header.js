import React, { useState, Fragment } from 'react';
import {useDispatch} from 'react-redux'
import { GetData, Nodes } from "Store";
import { withRouter, NavLink } from "react-router-dom";

import { USER_TYPE } from "../../common/constants";

import CustomerHeader from "./CustomerHeader";
import TaskerHeader from "./TaskerHeader";
import UserCorner from "./UserCorner";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";


import {componentDidUpdate} from "../../common/lifecycles";

const Header = (props) => {
    const { userType, userName } = props;


    return <div className="Header">
        <Navbar collapseOnSelect expand="lg" bg="dark" fixed="top" variant="dark">
            <Navbar.Brand>Cheetah</Navbar.Brand>
            {
                userType === USER_TYPE.CUSTOMER &&
                <CustomerHeader/>
            }
            {
                userType === USER_TYPE.TASKER &&
                <TaskerHeader/>
            }
            {
                userType ?
                    <UserCorner userName={userName} userType={userType}/>
                    :
                    <Nav>
                        <Navbar.Text>
                            <NavLink to={"/"}>
                                Login
                            </NavLink>
                        </Navbar.Text>
                        <Navbar.Text>
                            <NavLink to={"/register"}>
                                Register
                            </NavLink>
                        </Navbar.Text>
                    </Nav>
            }

        </Navbar>
            </div>
}

export default withRouter(Header);
