import React from 'react'
import { node } from 'prop-types'
import {createStore, compose, applyMiddleware} from 'redux'
import rootReducer, {Reducers} from 'Reducers'
import {Provider, useSelector, shallowEqual} from 'react-redux'
import thunk from 'redux-thunk';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer,
    composeEnhancer(applyMiddleware(thunk)),
)
function Store({ children }) {
    return (
        <Provider store={store}>
            {children}
        </Provider>
    )
}
Store.defaultProps = {
    children: null
}
Store.propTypes = {
    children: node
}

let nodeNames = {};

for (let key in Reducers) {
    nodeNames[key] = key;
}


export default Store;

export const Nodes = nodeNames;

export const GetData = (nodeName) => {
    if (!nodeNames[nodeName]) {
        throw "Invalid Node name!"
        return false;
    }
    return useSelector( state => state[nodeName], shallowEqual);
}
