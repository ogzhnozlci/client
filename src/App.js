import React, { useState } from 'react';
import { Route, Switch, withRouter} from "react-router-dom";
import { GetData, Nodes } from "Store";

import { setAuthorizationHeader } from "./common/xhr";

import Header from "Components/Header/Header";

import { Routes } from "./routes";
import { componentDidUpdate} from "Lifecycles";
import { USER_TYPE } from "./common/constants";

const classNames = require('classnames')

const App = (props) => {
    const loginData = GetData(Nodes.account)

    const [loggedIn, setLoggedIn] = useState(false);
    const [userName, setUsername] = useState(false);
    const [RoutesArr, setRoutesArr] = useState(Routes(false));

    componentDidUpdate(() => {
        let authData = loginData.register_login;
        let userType = authData.isTasker ? USER_TYPE.TASKER : USER_TYPE.CUSTOMER;

        setAuthorizationHeader(authData.token);
        setLoggedIn(userType);
        setUsername(authData.username);
        setRoutesArr(Routes(userType))

    }, [loginData.register_login]);

    componentDidUpdate(() => {
        props.history.push("/");
    }, [RoutesArr]);

    return <React.StrictMode>
            <div className="layout-container">
                    <div className={classNames("main")}>
                        <Header userType={loggedIn} userName={userName}/>
                        <div className="main">
                            <Switch>
                                {
                                    RoutesArr.map((RouteInfo,i)=>{
                                        if (RouteInfo.path) {
                                            return <Route key={i} path={RouteInfo.path} exact component={RouteInfo.component}/>
                                        } else {
                                            return <Route key={i} component={RouteInfo.component}/>
                                        }
                                    })
                                }
                            </Switch>
                        </div>
                    </div>
            </div>
        </React.StrictMode>
}

export default withRouter(App);
