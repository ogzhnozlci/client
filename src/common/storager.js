/**
 * @module common/storager
 * @desc Common methods for LocalStorager usage
 */

const getStoredValue = key => {
    return window.localStorage.getItem(key);
};

const setStoredValue = (key, value) => {
    return window.localStorage.setItem(key, value);
};

const removeStoredValue = key => {
    return window.localStorage.removeItem(key);
};

const clearAllStoredValue = () => {
    return window.localStorage.clear();
};


/**
 * Tools for LocalStorager set,get,remove,clear actions
 * @type {Object}
 * @property {Function} get - Get data from LocalStorager
 * @property {Function} set - Set data to LocalStorager
 * @property {Function} remove - Remove data from LocalStorager
 * @property {Function} clear - Clear All data on LocalStorager
 */
const Storager = {
    /**
     * @method
     * @param {String} key - Key name of value
     */
    get: function (key) {
        return getStoredValue(key);
    },
    /**
     * @method
     * @param {String} key - Key name
     * @param {String} value - Value of Key
     */
    set: function (key, value) {
        return setStoredValue(key, value);
    },
    /**
     * @method
     * @param {String} key - Key name of value
     */
    remove: function (key) {
        return removeStoredValue(key);
    },
    clear: function () {
        return clearAllStoredValue();
    }
};


export default Storager;
