import React, { useState, Fragment } from 'react';
import {useDispatch} from 'react-redux'
import { GetData, Nodes } from "Store";
import { NavLink } from "react-router-dom";


import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";


import {componentDidUpdate} from "../../common/lifecycles";

const TaskListItem = (props) => {
    const { data, isRequest, showRequest, isOwn, onBtnClick} = props;
    const loginData = GetData(Nodes.account)
    const isTasker = loginData.register_login.isTasker;


    return <Row>
                <Col>
                    <Card>
                        <Card.Header>
                            {data.category.title}
                        </Card.Header>
                        <Card.Body>
                            <Card.Title>{data.title}</Card.Title>
                            <Card.Text>
                                {data.content}
                            </Card.Text>
                            <Card.Text className={"text-right"}>
                                {data.creator.username}
                            </Card.Text>
                            <div className={"text-right"}>
                                {
                                    isTasker &&
                                    <Button onClick={()=>onBtnClick(data)} variant="primary">Make Request</Button>
                                }

                                {
                                    (isOwn && showRequest) &&
                                    <Button onClick={()=>onBtnClick(data)} variant="primary">Show Requests</Button>
                                }
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
}

export default TaskListItem;
