import React, { useState, Fragment } from 'react';
import { withRouter, NavLink } from "react-router-dom";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

const UserCorner = (props) => {
    const { userType, userName } = props;

    return <Nav>
                <Navbar.Text>
                    {"Welcome " + userType + " " + userName}
                </Navbar.Text>

            </Nav>
}

export default withRouter(UserCorner);
