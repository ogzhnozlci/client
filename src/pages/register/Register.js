import React, { useState } from 'react';
import {useDispatch} from 'react-redux'
import { GetData, Nodes } from "Store";
import Actions from "../../redux/actions";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

const Register = (props) => {
    const { t } = props;
    const dispatch = useDispatch()
    const loginData = GetData(Nodes.account)

    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [isTasker, setIsTasker] = useState(false);
    const [password, setPassword] = useState("");


    const validateForm = () => {
        return email.length > 0 && password.length > 5;
    }

    const handleSubmit = (event) => {
        let data = {
            username,
            email,
            password,
            isTasker
        }
        dispatch(Actions.account.registerAccount(data))
        event.preventDefault();
    }

    return <div className="Login">
                <h1>
                    REGISTER
                </h1>
                <Form onSubmit={handleSubmit}>
                    <Form.Group size="lg" controlId="username">
                        <Form.Label>Username</Form.Label>
                        <Form.Control
                            autoFocus
                            type="text"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group size="lg" controlId="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            type="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group size="lg" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formBasicCheckbox">
                        <Form.Check
                            type="checkbox"
                            label="I am a Tasker!"
                            onChange={(e) => setIsTasker(e.target.checked)}
                        />
                    </Form.Group>
                    <Button block size="lg" type="submit" disabled={!validateForm()}>
                        Login
                    </Button>
                </Form>
            </div>
}

export default Register;
