import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from "react-router-dom";

import './styles/main.scss';

import Store from "Store";

import App from "./App";

ReactDOM.render(
    <Store>
        <Router>
            <App/>
        </Router>
    </Store>,
    document.getElementById('root')
);
