import { XHR } from 'XHR';
import { TASK } from "../action_types";

const getAllTasks = () => {
    return async function(dispatch, getState) {

        let resp = await XHR.get("/task");

        dispatch({
            type: TASK.ALL_TASKS,
            payload: resp.data.data
        })
    };
}

const getRequestMadeTasks = () => {
    return async function(dispatch, getState) {

        let resp = await XHR.get("/tasks/requests/own");

        dispatch({
            type: TASK.REQUEST_MADE_TASKS,
            payload: resp.data.data
        })
    };
}

const getMyTasks = () => {
    return async function(dispatch, getState) {

        let resp = await XHR.get("/tasks/own");

        dispatch({
            type: TASK.MY_TASKS,
            payload: resp.data.data
        })
    };
}

const getRequestOfTask = (id) => {
    return async function(dispatch, getState) {

        let resp = await XHR.get("/task/requests/" + id);

        dispatch({
            type: TASK.REQUEST_OF_TASK,
            payload: resp.data.data
        })
    };
}

const makeRequestToTask = (id, value) => {
    return async function(dispatch, getState) {
        let data = {
            value
        }
        let resp = await XHR.post("/task/request/" + id, data);

        dispatch({
            type: TASK.MAKE_REQUEST,
            payload: resp.data.data
        })
    };
}

const getRequestedTasks = () => {
    return async function(dispatch, getState) {
        let resp = await XHR.get("/tasks/requests/own");

        dispatch({
            type: TASK.REQUESTED_TASKS,
            payload: resp.data.data
        })
    };
}

const getCategories = () => {
    return async function(dispatch, getState) {
        let resp = await XHR.get("/category");

        dispatch({
            type: TASK.CATEGORIES,
            payload: resp.data.data
        })
    };
}

const createTask = (data) => {
    return async function(dispatch, getState) {
        let resp = await XHR.post("/task", data);

        dispatch({
            type: TASK.CREATE_TASK,
            payload: resp.data.data
        })
    };
}


export default {
    getAllTasks,
    getMyTasks,
    getRequestMadeTasks,
    getRequestOfTask,
    makeRequestToTask,
    getRequestedTasks,
    getCategories,
    createTask,
}
