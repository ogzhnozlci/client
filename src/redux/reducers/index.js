import account from './account'
import task from './task'


import {combineReducers} from 'redux'

const ReducerList = {
    account,
    task,
}


const rootReducer = combineReducers(ReducerList)
export default rootReducer;

export const Reducers = ReducerList;
