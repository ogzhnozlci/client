import React, { useState } from 'react';
import {useDispatch} from 'react-redux'
import { GetData, Nodes } from "Store";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import ReqestedTaskList from "../../components/TaskList/ReqestedTaskList";

import {componentDidMount, componentDidUpdate} from "../../common/lifecycles";
import Actions from "../../redux/actions";

const TaskRequested = (props) => {
    const dispatch = useDispatch()
    const taskData = GetData(Nodes.task)
    const [tasks, setTasks] = useState([])

    componentDidMount(()=>{
        dispatch(Actions.task.getRequestMadeTasks());
    })

    componentDidUpdate(()=> {
        setTasks(taskData.request_made_tasks);
    }, [taskData.request_made_tasks])

    return <div className="TaskRequested">
                <Container>
                    <Row>
                        <Col>
                            <h1 className={"text-center"}>
                                TASKS
                            </h1>
                        </Col>
                    </Row>
                    <ReqestedTaskList data={tasks}/>
                </Container>

            </div>
}

export default TaskRequested;
