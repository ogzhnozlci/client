import React, { useState, Fragment } from 'react';
import { withRouter, NavLink } from "react-router-dom";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

const CustomerHeader = (props) => {

    return <Nav>
            <Navbar.Text>
                <NavLink to={"/"}>
                    All Tasks
                </NavLink>
            </Navbar.Text>
            <Navbar.Text>
                <NavLink to={"/mytasks"}>
                    My Tasks
                </NavLink>
            </Navbar.Text>
            <Navbar.Text>
                <NavLink to={"/createtask"}>
                    Create Task
                </NavLink>
            </Navbar.Text>
            </Nav>
}

export default withRouter(CustomerHeader);
