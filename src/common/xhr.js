import axios from 'axios';
import {
    BASE_URL,
    PROXY_BASE_URL,
    PROXY_TEST_BASE_URL,
    LOCAL_PROXY_BASE_URL,
    LOCAL_PROXY_TEST_BASE_URL,
    REFRESH_TOKEN_ENDPOINT
} from "Constants";

import Storager from 'Storager'


export const getBaseUrl = () => {
    return BASE_URL;
}

export let XHR = axios.create({
    baseURL: getBaseUrl(),
});

const handleResponse = (response) => {
    return response;
};

let isRefreshing;
let subscribers = [];
let successStatus = [200,201,204]

const handleError = (err) => {
    //return throw error
};

const subscribeTokenRefresh = (cb) => subscribers.push(cb);
const onRefreshed = () => subscribers.map(cb => cb());

const setInterceptors = () => {
    XHR.interceptors.response.use(handleResponse, handleError);
}

export const setAuthorizationHeader = (token) => {
    XHR = axios.create({
        baseURL: getBaseUrl(),
        headers: {'Authorization': token}
    });
    setInterceptors();
};


export const clearAuthorizationHeader = () => {
    XHR = axios.create({
        baseURL: getBaseUrl(),
    });

    setInterceptors();
};
