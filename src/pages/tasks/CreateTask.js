import React, { useState } from 'react';
import {useDispatch} from 'react-redux'
import { GetData, Nodes } from "Store";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";


import {componentDidMount, componentDidUpdate} from "../../common/lifecycles";

import Actions from "../../redux/actions";

const CreateTask = (props) => {
    const dispatch = useDispatch()
    const taskData = GetData(Nodes.task)

    const [categories, setCategories] = useState([]);

    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const [category, setCategory] = useState("");

    const [showSuccessModal, setShowSuccessModal] = useState(false);

    const handleSuccessClose = () => setShowSuccessModal(false);
    const handleSuccessShow = data => {
        setShowSuccessModal(true);
    }

    componentDidMount(()=>{
        dispatch(Actions.task.getCategories())
    })

    const handleSubmit = () => {
        let data = {
            title,
            content,
            category
        }

        dispatch(Actions.task.createTask(data));
        event.preventDefault();
    }

    componentDidUpdate(()=>{
        setCategories(taskData.categories)
    }, [taskData.categories])

    componentDidUpdate(()=>{
        handleSuccessShow();
    }, [taskData.create_task])

    componentDidUpdate(()=>{
        if (!showSuccessModal) {
            props.history.push("/mytasks")
        } else {
            setTimeout(()=>{
                handleSuccessClose();
            },2000)
        }
    },[showSuccessModal])

    return <div className="Task">
                <Container>
                    <Row>
                        <Col>
                            <h1 className={"text-center"}>
                                CREATE TASK
                            </h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form onSubmit={handleSubmit}>
                                <Form.Group size="lg" controlId="title">
                                    <Form.Label>Title</Form.Label>
                                    <Form.Control
                                        autoFocus
                                        type="text"
                                        value={title}
                                        onChange={(e) => setTitle(e.target.value)}
                                    />
                                </Form.Group>
                                <Form.Group size="lg" controlId="content">
                                    <Form.Label>Content</Form.Label>
                                    <Form.Control
                                        type="text"
                                        value={content}
                                        onChange={(e) => setContent(e.target.value)}
                                    />
                                </Form.Group>
                                <Form.Group controlId="exampleForm.ControlSelect1">
                                    <Form.Label>Category</Form.Label>
                                    <Form.Control
                                        as="select"
                                        onChange={(e) => setCategory(e.target.value)}
                                    >
                                        <option value={""}></option>
                                        {
                                            categories.map((cat,i)=> {
                                                return <option key={i} value={cat._id}>{cat.title}</option>
                                            })
                                        }
                                    </Form.Control>
                                </Form.Group>
                                <Button block size="lg" type="submit">
                                    Create Task
                                </Button>
                            </Form>
                        </Col>
                    </Row>
                    <Modal show={showSuccessModal} onHide={handleSuccessClose}>
                        <Modal.Header>
                            <Modal.Title>Success!</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <p>{"You created a new task '" + title + "'"}</p>
                        </Modal.Body>
                    </Modal>
                </Container>

            </div>
}

export default CreateTask;
