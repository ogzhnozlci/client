
import account from './account'
import task from './task'

const Actions = {
    account,
    task
}

export default Actions;
