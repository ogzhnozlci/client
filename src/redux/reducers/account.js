import { ACCOUNT } from "../action_types";

const account = (state = {}, action) => {
    switch(action.type){
        case ACCOUNT.REGISTER_LOGIN:
            return {
                ...state,
                register_login: action.payload
            }
        default:
            return state
    }
}
export default account;
