import { TASK } from "../action_types";

const account = (state = {}, action) => {
    switch(action.type){
        case TASK.ALL_TASKS:
            return {
                ...state,
                all_task: action.payload
            }
        case TASK.REQUEST_MADE_TASKS:
            return {
                ...state,
                request_made_tasks: action.payload
            }
        case TASK.MY_TASKS:
            return {
                ...state,
                my_tasks: action.payload
            }
        case TASK.REQUEST_OF_TASK:
            return {
                ...state,
                requests_of_task: action.payload
            }
        case TASK.MAKE_REQUEST:
            return {
                ...state,
                make_request: action.payload
            }
        case TASK.REQUESTED_TASKS:
            return {
                ...state,
                requested_tasks: action.payload
            }
        case TASK.CATEGORIES:
            return {
                ...state,
                categories: action.payload
            }
        case TASK.CREATE_TASK:
            return {
                ...state,
                create_task: action.payload
            }
        default:
            return state
    }
}
export default account;
