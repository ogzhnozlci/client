import React, { useState, Fragment } from 'react';
import {useDispatch} from 'react-redux'
import { GetData, Nodes } from "Store";


import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";

import TaskListItem from "./TaskListItem";
import {componentDidUpdate} from "../../common/lifecycles";
import Actions from "../../redux/actions";


const TaskList = (props) => {
    const { data } = props;
    const dispatch = useDispatch();
    const taskData = GetData(Nodes.task)

    const [showRequetsModal, setShowRequetsModal] = useState(false);
    const [modalContent, setModalContent] = useState(false);
    const [requestValue, setRequestValue] = useState(false);
    const [showSuccessModal, setShowSuccessModal] = useState(false);

    const handleClose = () => setShowRequetsModal(false);
    const handleShow = data => {
        setModalContent(data)
        setShowRequetsModal(true);
    }

    const handleSuccessClose = () => setShowSuccessModal(false);
    const handleSuccessShow = data => {
        setShowSuccessModal(true);
    }


    const makeRequest = () => {
        dispatch(Actions.task.makeRequestToTask(modalContent._id, requestValue));
    }

    componentDidUpdate(()=>{
        handleClose();
        handleSuccessShow(true)
    },[taskData.make_request])

    componentDidUpdate(()=>{
       if (!showSuccessModal) {
           setModalContent(false)
           setRequestValue(false)
       } else {
           setTimeout(()=>{
               handleSuccessClose();
           },2000)
       }
    },[showSuccessModal])

    return <div className="TaskList">
                {
                    data.map((task,i)=>{
                        return <TaskListItem onBtnClick={handleShow} data={task} key={i}/>
                    })
                }

        <Modal show={showRequetsModal} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>{modalContent.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group size="lg" controlId="value">
                    <Form.Label>Value (USD)</Form.Label>
                    <Form.Control
                        type="number"
                        value={requestValue}
                        onChange={(e) => setRequestValue(e.target.value)}
                    />
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={makeRequest} variant="primary">Make Request</Button>
            </Modal.Footer>
        </Modal>


        <Modal show={showSuccessModal} onHide={handleSuccessClose}>
            <Modal.Header>
                <Modal.Title>Success!</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>{"You made request to " + modalContent.title}</p>
                <p>{"Value: " + requestValue + " USD"}</p>
            </Modal.Body>
        </Modal>
            </div>
}

export default TaskList;
